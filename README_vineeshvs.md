Trying out Goldmine tool
========================

* PRE-REQUISITE: 

  docker installation is required(followed the recommended method of *install using repository*).
    https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

    Note: Installing from .deb file didnt' work (maybe I installed the wrong .deb version/file. Not sure).

* Issue with choosing the right shell environment

  ERROR

      bash: Bad Substitution
    
  SOLUTION
  
  Make build_docker.sh file executable, and run using ./build_docker.sh instead of sh build_docker.sh

      chmod +x build_docker.sh
      ./build_docker.sh
    
    Reference:
    
    https://stackoverflow.com/questions/20615217/bash-bad-substitution

* Docker was not running unless it is run in *sudo* mode.

    ERROR

      $ source build_docker.sh 
      Setting DOCKER_BUILDKIT environment variables..
      failed to dial gRPC: cannot connect to the Docker daemon. Is 'docker daemon' running on this host?: dial unix /var/run/docker.sock: connect: permission denied
      docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/create: dial unix /var/run/docker.sock: connect: permission denied.
      See 'docker run --help'.

    SOLUTION 
    
    build_docker.sh file had to be run in sudo mode.


      $ sudo ./build_docker.sh 
      Setting DOCKER_BUILDKIT environment variables..
      ...

* Python version issues while running run_goldmine.sh

  ERROR

  'python' command was calling python3.x in my terminal.

  SOLUTION

  Solution: Replace 'python' with 'python2' in run_goldmine.sh

* Error while running run_goldmine.sh: "Too many values to unpack"

  ERROR

                                                                  ______      __    ____  ____          
                                                                  / ____/___  / /___/ /  |/  (_)___  ___ 
                                                                / / __/ __ \/ / __  / /|_/ / / __ \/ _ \
                                                                / /_/ / /_/ / / /_/ / /  / / / / / /  __/
                                                                \____/\____/_/\__,_/_/  /_/_/_/ /_/\___/ 
                                                                                                        

      [INFO]--> User specified assertion file does not exist. Continuing with auto-generated assertions 
      [INFO]--> No Mining Engine specified. Engine specified in the goldmine.cfg will be used 
      [WARN]--> VCS Executable Not Found. 
      [WARN]--> IFV Executable Not Found. Assertion Verification won't be possible. 
                                                                    ########################################                                                              
                                                                    ##  Mining for design: ibex_id_stage  ##                                                              
                                                                    ########################################                                                              
                                                                        ____                  _            
                                                                        / __ \____ ___________(_)___  ____ _
                                                                      / /_/ / __ `/ ___/ ___/ / __ \/ __ `/
                                                                      / ____/ /_/ / /  (__  ) / / / / /_/ / 
                                                                    /_/    \__,_/_/  /____/_/_/ /_/\__, /  
                                                                                                  /____/   

      Parsing: ibex_id_stage.v...
      Traceback (most recent call last):
        File "../src/goldmine.py", line 117, in <module>
          CMD_LINE_OPTIONS['INCLUDE'], [])
        File "/home/vineesh/trunk/research/others/goldminer/src/verilog.py", line 1576, in parse_verilog
          preprocess_define=define)
      ValueError: need more than 2 values to unpack

  SOLUTION

  Replace ~/src/pyverilog/ with the one downloaded from https://www.dropbox.com/s/e1wf6vh64v9tpf1/pyverilog_customized.tar.gz?dl=0

* Keyerror

  ERROR

                                                                      ______      __    ____  ____          
                                                                      / ____/___  / /___/ /  |/  (_)___  ___ 
                                                                    / / __/ __ \/ / __  / /|_/ / / __ \/ _ \
                                                                    / /_/ / /_/ / / /_/ / /  / / / / / /  __/
                                                                    \____/\____/_/\__,_/_/  /_/_/_/ /_/\___/ 
                                                                                                            

      [INFO]--> User specified assertion file does not exist. Continuing with auto-generated assertions 
      [INFO]--> No Mining Engine specified. Engine specified in the goldmine.cfg will be used 
      [WARN]--> VCS Executable Not Found. 
      [WARN]--> IFV Executable Not Found. Assertion Verification won't be possible. 
                                                                    ########################################                                                              
                                                                    ##  Mining for design: ibex_id_stage  ##                                                              
                                                                    ########################################                                                              
                                                                        ____                  _            
                                                                        / __ \____ ___________(_)___  ____ _
                                                                      / /_/ / __ `/ ___/ ___/ / __ \/ __ `/
                                                                      / ____/ /_/ / /  (__  ) / / / / /_/ / 
                                                                    /_/    \__,_/_/  /____/_/_/ /_/\__, /  
                                                                                                  /____/   

      Parsing: ibex_id_stage.v...
      Parsing: ibex_decoder.v...
      Parsing: ibex_controller.v...
      Parsing: ibex_register_file_ff.v...


                                                              ________      __                     __  _            
                                                              / ____/ /___ _/ /_  ____  _________ _/ /_(_)___  ____ _
                                                            / __/ / / __ `/ __ \/ __ \/ ___/ __ `/ __/ / __ \/ __ `/
                                                            / /___/ / /_/ / /_/ / /_/ / /  / /_/ / /_/ / / / / /_/ / 
                                                          /_____/_/\__,_/_.___/\____/_/   \__,_/\__/_/_/ /_/\__, /  
                                                                                                            /____/   

      Elaborating: ibex_id_stage
      Traceback (most recent call last):
        File "../src/goldmine.py", line 170, in <module>
          clks, Params, Ports)
        File "/home/vineesh/trunk/research/others/goldminer/src/static_analysis.py", line 596, in CDFG
          cdfg_type = CDFG.node[CDFG.nodes()[0]]['typ']
        File "/usr/local/lib/python2.7/dist-packages/networkx/classes/reportviews.py", line 178, in __getitem__
          return self._nodes[n]
      KeyError: 0

  SOLUTION

    I was using Networkx 2.2 . As per the requirements, I am supposed to version 1.11 . Used the following command to install Networkx1.1

      sudo python2 -m pip install networkx==1.11