ERROR

    $ sh run_goldmine.sh 
                                                                    ______      __    ____  ____          
                                                                    / ____/___  / /___/ /  |/  (_)___  ___ 
                                                                  / / __/ __ \/ / __  / /|_/ / / __ \/ _ \
                                                                  / /_/ / /_/ / / /_/ / /  / / / / / /  __/
                                                                  \____/\____/_/\__,_/_/  /_/_/_/ /_/\___/ 
                                                                                                          

    [INFO]--> User specified assertion file does not exist. Continuing with auto-generated assertions 
    [INFO]--> No Mining Engine specified. Engine specified in the goldmine.cfg will be used 
    [WARN]--> VCS Executable Not Found. 
    [WARN]--> IFV Executable Not Found. Assertion Verification won't be possible. 
                                                              ##################################################                                                         
                                                              ##  Mining for design: ibex_compressed_decoder  ##                                                         
                                                              ##################################################                                                         
                                                                      ____                  _            
                                                                      / __ \____ ___________(_)___  ____ _
                                                                    / /_/ / __ `/ ___/ ___/ / __ \/ __ `/
                                                                    / ____/ /_/ / /  (__  ) / / / / /_/ / 
                                                                  /_/    \__,_/_/  /____/_/_/ /_/\__, /  
                                                                                                /____/   

    Parsing: ibex_compressed_decoder.v...


                                                            ________      __                     __  _            
                                                            / ____/ /___ _/ /_  ____  _________ _/ /_(_)___  ____ _
                                                          / __/ / / __ `/ __ \/ __ \/ ___/ __ `/ __/ / __ \/ __ `/
                                                          / /___/ / /_/ / /_/ / /_/ / /  / /_/ / /_/ / / / / /_/ / 
                                                        /_____/_/\__,_/_.___/\____/_/   \__,_/\__/_/_/ /_/\__, /  
                                                                                                          /____/   

    Elaborating: ibex_compressed_decoder
                                                                        __    _       __   _            
                                                                      / /   (_)___  / /__(_)___  ____ _
                                                                      / /   / / __ \/ //_/ / __ \/ __ `/
                                                                    / /___/ / / / / ,< / / / / / /_/ / 
                                                                    /_____/_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                              /____/   

                                                                ______   ____              __   _            
                                                                / ____/  / __ \____ _____  / /__(_)___  ____ _
                                                              / /      / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / /___   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                              \____/  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                    /____/   

    Statically analyzing target variable: is_compressed_o
    Statically analyzing target variable: instr_o
    Statically analyzing target variable: illegal_instr_o


    [INFO]--> Total time for parsing, ranking: 0:00:01.118796 
    [INFO]--> Peak Memory Usage for parsing, ranking: 521.17 MB 
                                                                        _____             _            
                                                                      / ___/____ __   __(_)___  ____ _
                                                                      \__ \/ __ `/ | / / / __ \/ __ `/
                                                                      ___/ / /_/ /| |/ / / / / / /_/ / 
                                                                    /____/\__,_/ |___/_/_/ /_/\__, /  
                                                                                              /____/   

    Saving static analysis info for: ibex_compressed_decoder


    Saving static analysis info for complete: ibex_compressed_decoder
    [WARN]--> Cannot remove a non-existent directory: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/static/cone 
    Saving cone for the target variable: is_compressed_o
    Saving cone for the target variable: instr_o
    Saving cone for the target variable: illegal_instr_o
                                                                    ____        __           ______         
                                                                  / __ \____ _/ /_____ _   / ____/__  ____ 
                                                                  / / / / __ `/ __/ __ `/  / / __/ _ \/ __ \
                                                                / /_/ / /_/ / /_/ /_/ /  / /_/ /  __/ / / /
                                                                /_____/\__,_/\__/\__,_/   \____/\___/_/ /_/ 
                                                                                                            

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/ibex_compressed_decoder_bench.v 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/ibex_compressed_decoder.vcd 
    [INFO]--> Simulating with: iverilog 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/ibex_compressed_decoder_compile.log 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/ibex_compressed_decoder_err.log 
    [INFO]--> Compile Time: 0:00:01.010967 
    [INFO]--> Compile Memory Usage: 4.4 MB 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/ibex_compressed_decoder_run.log 
    [INFO]--> Run Time: 0:00:01.010967 
    [INFO]--> Run Memory Usage: 4.4 MB 


                                                                      ____     ____                     
                                                                      / __ \   / __ \____ ______________ 
                                                                    / / / /  / /_/ / __ `/ ___/ ___/ _ \
                                                                    / /_/ /  / ____/ /_/ / /  (__  )  __/
                                                                  /_____/  /_/    \__,_/_/  /____/\___/ 
                                                                                                        

    [INFO]--> Parsing VCD file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/ibex_compressed_decoder.vcd 


    [INFO]--> Parsing VCD file generated on: Mon Sep 28 21:14:43 2020 
    [INFO]--> VCD file created by the tool: Icarus Verilog 
    [INFO]--> Design was simulated using timescale: 1ps 


    [INFO]--> Parsing trace data 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Creating data frames for Combinational circuit type 


    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)


    [INFO]--> Converting time shifted mine data in dataframes 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)


    [INFO]--> Validating simulation data 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:02)
    [INFO]--> Number of added unique data examples: 1000 
    [INFO]--> Number of valid examples: 725 
    [INFO]--> Number of invalid examples: 275 


    [INFO]--> Total time to parse VCD file: 0:00:03.428990 
                                                                          __  ____       _            
                                                                        /  |/  (_)___  (_)___  ____ _
                                                                        / /|_/ / / __ \/ / __ \/ __ `/
                                                                      / /  / / / / / / / / / / /_/ / 
                                                                      /_/  /_/_/_/ /_/_/_/ /_/\__, /  
                                                                                            /____/   

    [INFO]--> Engine used for mining: prism 


                                                                ##############################################                                                           
                                                                ##  Mining for target :--> is_compressed_o  ##                                                           
                                                                ##############################################                                                           
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 1000 
    [INFO]--> Number of valid examples: 999 
    [INFO]--> Number of invalid examples: 1 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/verif/prism/is_compressed_o/is_compressed_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/verif/prism/is_compressed_o/is_compressed_o.gold 


                                                                ##############################################                                                           
                                                                ##  Mining for target :--> illegal_instr_o  ##                                                           
                                                                ##############################################                                                           
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 1000 
    [INFO]--> Number of valid examples: 999 
    [INFO]--> Number of invalid examples: 1 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/verif/prism/illegal_instr_o/illegal_instr_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_compressed_decoder/verif/prism/illegal_instr_o/illegal_instr_o.gold 
    [INFO]--> Total time to mine: 0:00:28.712858 


    [INFO]--> Total Run Time: 0:00:43.571360 
                                                                    ______      __    ____  ____          
                                                                    / ____/___  / /___/ /  |/  (_)___  ___ 
                                                                  / / __/ __ \/ / __  / /|_/ / / __ \/ _ \
                                                                  / /_/ / /_/ / / /_/ / /  / / / / / /  __/
                                                                  \____/\____/_/\__,_/_/  /_/_/_/ /_/\___/ 
                                                                                                          

    [INFO]--> User specified assertion file does not exist. Continuing with auto-generated assertions 
    [INFO]--> No Mining Engine specified. Engine specified in the goldmine.cfg will be used 
    [WARN]--> VCS Executable Not Found. 
    [WARN]--> IFV Executable Not Found. Assertion Verification won't be possible. 
                                                                  ##########################################                                                             
                                                                  ##  Mining for design: ibex_controller  ##                                                             
                                                                  ##########################################                                                             
                                                                      ____                  _            
                                                                      / __ \____ ___________(_)___  ____ _
                                                                    / /_/ / __ `/ ___/ ___/ / __ \/ __ `/
                                                                    / ____/ /_/ / /  (__  ) / / / / /_/ / 
                                                                  /_/    \__,_/_/  /____/_/_/ /_/\__, /  
                                                                                                /____/   

    Parsing: ibex_controller.v...


                                                            ________      __                     __  _            
                                                            / ____/ /___ _/ /_  ____  _________ _/ /_(_)___  ____ _
                                                          / __/ / / __ `/ __ \/ __ \/ ___/ __ `/ __/ / __ \/ __ `/
                                                          / /___/ / /_/ / /_/ / /_/ / /  / /_/ / /_/ / / / / /_/ / 
                                                        /_____/_/\__,_/_.___/\____/_/   \__,_/\__/_/_/ /_/\__, /  
                                                                                                          /____/   

    Elaborating: ibex_controller
                                                                        __    _       __   _            
                                                                      / /   (_)___  / /__(_)___  ____ _
                                                                      / /   / / __ \/ //_/ / __ \/ __ `/
                                                                    / /___/ / / / / ,< / / / / / /_/ / 
                                                                    /_____/_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                              /____/   

                                                                ______   ____              __   _            
                                                                / ____/  / __ \____ _____  / /__(_)___  ____ _
                                                              / /      / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / /___   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                              \____/  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                    /____/   

    Statically analyzing target variable: id_in_ready_o
    Statically analyzing target variable: pc_set_o
    Statically analyzing target variable: instr_valid_clear_o
    Statically analyzing target variable: csr_save_id_o
    Statically analyzing target variable: csr_mtval_o
    Statically analyzing target variable: debug_cause_o
    Statically analyzing target variable: exc_cause_o
    Statically analyzing target variable: perf_jump_o
    Statically analyzing target variable: csr_restore_mret_id_o
    Statically analyzing target variable: perf_tbranch_o
    Statically analyzing target variable: csr_save_if_o
    Statically analyzing target variable: instr_req_o
    Statically analyzing target variable: csr_restore_dret_id_o
    Statically analyzing target variable: debug_csr_save_o
    Statically analyzing target variable: pc_mux_o
    Statically analyzing target variable: exc_pc_mux_o
    Statically analyzing target variable: ctrl_busy_o
    Statically analyzing target variable: csr_save_cause_o
    Statically analyzing target variable: debug_mode_o


    [INFO]--> Total time for parsing, ranking: 0:00:01.716443 
    [INFO]--> Peak Memory Usage for parsing, ranking: 524.43 MB 
                                                                        _____             _            
                                                                      / ___/____ __   __(_)___  ____ _
                                                                      \__ \/ __ `/ | / / / __ \/ __ `/
                                                                      ___/ / /_/ /| |/ / / / / / /_/ / 
                                                                    /____/\__,_/ |___/_/_/ /_/\__, /  
                                                                                              /____/   

    Saving static analysis info for: ibex_controller


    Saving static analysis info for complete: ibex_controller
    [WARN]--> Cannot remove a non-existent directory: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/static/cone 
    Saving cone for the target variable: id_in_ready_o
    Saving cone for the target variable: pc_set_o
    Saving cone for the target variable: instr_valid_clear_o
    Saving cone for the target variable: csr_save_id_o
    Saving cone for the target variable: csr_mtval_o
    Saving cone for the target variable: debug_cause_o
    Saving cone for the target variable: exc_cause_o
    Saving cone for the target variable: perf_jump_o
    Saving cone for the target variable: csr_restore_mret_id_o
    Saving cone for the target variable: perf_tbranch_o
    Saving cone for the target variable: csr_save_if_o
    Saving cone for the target variable: instr_req_o
    Saving cone for the target variable: csr_restore_dret_id_o
    Saving cone for the target variable: debug_csr_save_o
    Saving cone for the target variable: pc_mux_o
    Saving cone for the target variable: debug_mode_o
    Saving cone for the target variable: ctrl_busy_o
    Saving cone for the target variable: csr_save_cause_o
    Saving cone for the target variable: exc_pc_mux_o
                                                                    ____        __           ______         
                                                                  / __ \____ _/ /_____ _   / ____/__  ____ 
                                                                  / / / / __ `/ __/ __ `/  / / __/ _ \/ __ \
                                                                / /_/ / /_/ / /_/ /_/ /  / /_/ /  __/ / / /
                                                                /_____/\__,_/\__/\__,_/   \____/\___/_/ /_/ 
                                                                                                            

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/ibex_controller_bench.v 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/ibex_controller.vcd 
    [INFO]--> Simulating with: iverilog 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/ibex_controller_compile.log 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/ibex_controller_err.log 
    [INFO]--> Compile Time: 0:00:01.017793 
    [INFO]--> Compile Memory Usage: 4.4 MB 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/ibex_controller_run.log 
    [INFO]--> Run Time: 0:00:01.017793 
    [INFO]--> Run Memory Usage: 4.4 MB 


                                                                      ____     ____                     
                                                                      / __ \   / __ \____ ______________ 
                                                                    / / / /  / /_/ / __ `/ ___/ ___/ _ \
                                                                    / /_/ /  / ____/ /_/ / /  (__  )  __/
                                                                  /_____/  /_/    \__,_/_/  /____/\___/ 
                                                                                                        

    [INFO]--> Parsing VCD file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/ibex_controller.vcd 


    [INFO]--> Parsing VCD file generated on: Mon Sep 28 21:15:51 2020 
    [INFO]--> VCD file created by the tool: Icarus Verilog 
    [INFO]--> Design was simulated using timescale: 1ps 


    [INFO]--> Parsing trace data 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:02)
    [INFO]--> Creating time shifted data for Sequential circuit type 


    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)


    [INFO]--> Converting time shifted mine data in dataframes 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:01)


    [INFO]--> Validating simulation data 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:16)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 399 
    [INFO]--> Number of invalid examples: 600 


    [INFO]--> Total time to parse VCD file: 0:00:21.465441 
                                                                          __  ____       _            
                                                                        /  |/  (_)___  (_)___  ____ _
                                                                        / /|_/ / / __ \/ / __ \/ __ `/
                                                                      / /  / / / / / / / / / / /_/ / 
                                                                      /_/  /_/_/_/ /_/_/_/ /_/\__, /  
                                                                                            /____/   

    [INFO]--> Engine used for mining: prism 


                                                                ############################################                                                            
                                                                ##  Mining for target :--> id_in_ready_o  ##                                                            
                                                                ############################################                                                            
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                      ___       ____              __   _            
                      /   |     / __ \____ _____  / /__(_)___  ____ _
                    / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                    / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                  /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                            /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/id_in_ready_o/id_in_ready_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/id_in_ready_o/id_in_ready_o.gold 


                        #######################################                     
                        ##  Mining for target :--> pc_set_o  ##                     
                        #######################################                     
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/pc_set_o/pc_set_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/pc_set_o/pc_set_o.gold 


                                                              ##################################################                                                         
                                                              ##  Mining for target :--> instr_valid_clear_o  ##                                                         
                                                              ##################################################                                                         
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/instr_valid_clear_o/instr_valid_clear_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/instr_valid_clear_o/instr_valid_clear_o.gold 


                                                                ############################################                                                            
                                                                ##  Mining for target :--> csr_save_id_o  ##                                                            
                                                                ############################################                                                            
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_save_id_o/csr_save_id_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_save_id_o/csr_save_id_o.gold 


                                                            ####################################################                                                        
                                                            ##  Mining for target :--> csr_restore_mret_id_o  ##                                                        
                                                            ####################################################                                                        
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_restore_mret_id_o/csr_restore_mret_id_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_restore_mret_id_o/csr_restore_mret_id_o.gold 


                                                                  ##########################################                                                             
                                                                  ##  Mining for target :--> perf_jump_o  ##                                                             
                                                                  ##########################################                                                             
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Target value did not change in valid data rows. Mining cannot continue 


                                                                #############################################                                                            
                                                                ##  Mining for target :--> perf_tbranch_o  ##                                                            
                                                                #############################################                                                            
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Target value did not change in valid data rows. Mining cannot continue 


                                                                ############################################                                                            
                                                                ##  Mining for target :--> csr_save_if_o  ##                                                            
                                                                ############################################                                                            
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_save_if_o/csr_save_if_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_save_if_o/csr_save_if_o.gold 


                                                                  ##########################################                                                             
                                                                  ##  Mining for target :--> instr_req_o  ##                                                             
                                                                  ##########################################                                                             
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Target value did not change in valid data rows. Mining cannot continue 


                                                            ####################################################                                                        
                                                            ##  Mining for target :--> csr_restore_dret_id_o  ##                                                        
                                                            ####################################################                                                        
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_restore_dret_id_o/csr_restore_dret_id_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_restore_dret_id_o/csr_restore_dret_id_o.gold 


                                                              ###############################################                                                           
                                                              ##  Mining for target :--> debug_csr_save_o  ##                                                           
                                                              ###############################################                                                           
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/debug_csr_save_o/debug_csr_save_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/debug_csr_save_o/debug_csr_save_o.gold 


                                                                  ##########################################                                                             
                                                                  ##  Mining for target :--> ctrl_busy_o  ##                                                             
                                                                  ##########################################                                                             
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Target value did not change in valid data rows. Mining cannot continue 


                                                              ###############################################                                                           
                                                              ##  Mining for target :--> csr_save_cause_o  ##                                                           
                                                              ###############################################                                                           
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_save_cause_o/csr_save_cause_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/csr_save_cause_o/csr_save_cause_o.gold 


                                                                ###########################################                                                             
                                                                ##  Mining for target :--> debug_mode_o  ##                                                             
                                                                ###########################################                                                             
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 993 
    [INFO]--> Number of invalid examples: 6 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/debug_mode_o/debug_mode_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_controller/verif/prism/debug_mode_o/debug_mode_o.gold 
    [INFO]--> Total time to mine: 0:06:04.899442 


    [INFO]--> Total Run Time: 0:07:01.769148 
                                                                    ______      __    ____  ____          
                                                                    / ____/___  / /___/ /  |/  (_)___  ___ 
                                                                  / / __/ __ \/ / __  / /|_/ / / __ \/ _ \
                                                                  / /_/ / /_/ / / /_/ / /  / / / / / /  __/
                                                                  \____/\____/_/\__,_/_/  /_/_/_/ /_/\___/ 
                                                                                                          

    [INFO]--> User specified assertion file does not exist. Continuing with auto-generated assertions 
    [INFO]--> No Mining Engine specified. Engine specified in the goldmine.cfg will be used 
    [WARN]--> VCS Executable Not Found. 
    [WARN]--> IFV Executable Not Found. Assertion Verification won't be possible. 
                                                                ############################################                                                            
                                                                ##  Mining for design: ibex_multdiv_slow  ##                                                            
                                                                ############################################                                                            
                                                                      ____                  _            
                                                                      / __ \____ ___________(_)___  ____ _
                                                                    / /_/ / __ `/ ___/ ___/ / __ \/ __ `/
                                                                    / ____/ /_/ / /  (__  ) / / / / /_/ / 
                                                                  /_/    \__,_/_/  /____/_/_/ /_/\__, /  
                                                                                                /____/   

    Parsing: ibex_multdiv_slow.v...


                                                            ________      __                     __  _            
                                                            / ____/ /___ _/ /_  ____  _________ _/ /_(_)___  ____ _
                                                          / __/ / / __ `/ __ \/ __ \/ ___/ __ `/ __/ / __ \/ __ `/
                                                          / /___/ / /_/ / /_/ / /_/ / /  / /_/ / /_/ / / / / /_/ / 
                                                        /_____/_/\__,_/_.___/\____/_/   \__,_/\__/_/_/ /_/\__, /  
                                                                                                          /____/   

    Elaborating: ibex_multdiv_slow
                                                                        __    _       __   _            
                                                                      / /   (_)___  / /__(_)___  ____ _
                                                                      / /   / / __ \/ //_/ / __ \/ __ `/
                                                                    / /___/ / / / / ,< / / / / / /_/ / 
                                                                    /_____/_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                              /____/   

                                                                ______   ____              __   _            
                                                                / ____/  / __ \____ _____  / /__(_)___  ____ _
                                                              / /      / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / /___   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                              \____/  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                    /____/   

    Statically analyzing target variable: multdiv_result_o
    Statically analyzing target variable: alu_operand_a_o
    Statically analyzing target variable: alu_operand_b_o
    Statically analyzing target variable: valid_o


    [INFO]--> Total time for parsing, ranking: 0:00:01.221837 
    [INFO]--> Peak Memory Usage for parsing, ranking: 521.43 MB 
                                                                        _____             _            
                                                                      / ___/____ __   __(_)___  ____ _
                                                                      \__ \/ __ `/ | / / / __ \/ __ `/
                                                                      ___/ / /_/ /| |/ / / / / / /_/ / 
                                                                    /____/\__,_/ |___/_/_/ /_/\__, /  
                                                                                              /____/   

    Saving static analysis info for: ibex_multdiv_slow


    Saving static analysis info for complete: ibex_multdiv_slow
    [WARN]--> Cannot remove a non-existent directory: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/static/cone 
    Saving cone for the target variable: alu_operand_a_o
    Saving cone for the target variable: multdiv_result_o
    Saving cone for the target variable: alu_operand_b_o
    Saving cone for the target variable: valid_o
                                                                    ____        __           ______         
                                                                  / __ \____ _/ /_____ _   / ____/__  ____ 
                                                                  / / / / __ `/ __/ __ `/  / / __/ _ \/ __ \
                                                                / /_/ / /_/ / /_/ /_/ /  / /_/ /  __/ / / /
                                                                /_____/\__,_/\__/\__,_/   \____/\___/_/ /_/ 
                                                                                                            

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/ibex_multdiv_slow_bench.v 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/ibex_multdiv_slow.vcd 
    [INFO]--> Simulating with: iverilog 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/ibex_multdiv_slow_compile.log 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/ibex_multdiv_slow_err.log 
    [INFO]--> Compile Time: 0:00:01.006459 
    [INFO]--> Compile Memory Usage: 4.4 MB 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/ibex_multdiv_slow_run.log 
    [INFO]--> Run Time: 0:00:01.006459 
    [INFO]--> Run Memory Usage: 4.4 MB 


                                                                      ____     ____                     
                                                                      / __ \   / __ \____ ______________ 
                                                                    / / / /  / /_/ / __ `/ ___/ ___/ _ \
                                                                    / /_/ /  / ____/ /_/ / /  (__  )  __/
                                                                  /_____/  /_/    \__,_/_/  /____/\___/ 
                                                                                                        

    [INFO]--> Parsing VCD file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/ibex_multdiv_slow.vcd 


    [INFO]--> Parsing VCD file generated on: Mon Sep 28 21:22:35 2020 
    [INFO]--> VCD file created by the tool: Icarus Verilog 
    [INFO]--> Design was simulated using timescale: 1ps 


    [INFO]--> Parsing trace data 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:10)
    [INFO]--> Creating time shifted data for Sequential circuit type 


    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:05)


    [INFO]--> Converting time shifted mine data in dataframes 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:04)


    [INFO]--> Validating simulation data 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:08)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 0 
    [INFO]--> Number of invalid examples: 999 


    [INFO]--> Total time to parse VCD file: 0:00:29.116640 
                                                                          __  ____       _            
                                                                        /  |/  (_)___  (_)___  ____ _
                                                                        / /|_/ / / __ \/ / __ \/ __ `/
                                                                      / /  / / / / / / / / / / /_/ / 
                                                                      /_/  /_/_/_/ /_/_/_/ /_/\__, /  
                                                                                            /____/   

    [INFO]--> Engine used for mining: prism 


                                                                    ######################################                                                               
                                                                    ##  Mining for target :--> valid_o  ##                                                               
                                                                    ######################################                                                               
    [INFO]--> Constructing target specific data frame for mining.. 
    Processed: 100% [>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>] (Time: 0:00:00)
    [INFO]--> Number of added unique data examples: 999 
    [INFO]--> Number of valid examples: 955 
    [INFO]--> Number of invalid examples: 44 


    [INFO]--> Formal verification of mined assertions was skipped as per user request 
                                                                ___       ____              __   _            
                                                                /   |     / __ \____ _____  / /__(_)___  ____ _
                                                              / /| |    / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / ___ |   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                            /_/  |_|  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                      /____/   

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/verif/prism/valid_o/valid_o.cone 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_multdiv_slow/verif/prism/valid_o/valid_o.gold 
    [INFO]--> Total time to mine: 0:00:09.680162 


    [INFO]--> Total Run Time: 0:00:57.086390 
                                                                    ______      __    ____  ____          
                                                                    / ____/___  / /___/ /  |/  (_)___  ___ 
                                                                  / / __/ __ \/ / __  / /|_/ / / __ \/ _ \
                                                                  / /_/ / /_/ / / /_/ / /  / / / / / /  __/
                                                                  \____/\____/_/\__,_/_/  /_/_/_/ /_/\___/ 
                                                                                                          

    [INFO]--> User specified assertion file does not exist. Continuing with auto-generated assertions 
    [INFO]--> No Mining Engine specified. Engine specified in the goldmine.cfg will be used 
    [WARN]--> VCS Executable Not Found. 
    [WARN]--> IFV Executable Not Found. Assertion Verification won't be possible. 
                                                                  ########################################                                                              
                                                                  ##  Mining for design: ibex_id_stage  ##                                                              
                                                                  ########################################                                                              
                                                                      ____                  _            
                                                                      / __ \____ ___________(_)___  ____ _
                                                                    / /_/ / __ `/ ___/ ___/ / __ \/ __ `/
                                                                    / ____/ /_/ / /  (__  ) / / / / /_/ / 
                                                                  /_/    \__,_/_/  /____/_/_/ /_/\__, /  
                                                                                                /____/   

    Parsing: ibex_id_stage.v...
    Parsing: ibex_decoder.v...
    Parsing: ibex_controller.v...
    Parsing: ibex_register_file_ff.v...


                                                            ________      __                     __  _            
                                                            / ____/ /___ _/ /_  ____  _________ _/ /_(_)___  ____ _
                                                          / __/ / / __ `/ __ \/ __ \/ ___/ __ `/ __/ / __ \/ __ `/
                                                          / /___/ / /_/ / /_/ / /_/ / /  / /_/ / /_/ / / / / /_/ / 
                                                        /_____/_/\__,_/_.___/\____/_/   \__,_/\__/_/_/ /_/\__, /  
                                                                                                          /____/   

    Elaborating: ibex_id_stage
    Elaborating: ibex_controller
    Elaborating: ibex_decoder
    Elaborating: ibex_register_file
                                                                        __    _       __   _            
                                                                      / /   (_)___  / /__(_)___  ____ _
                                                                      / /   / / __ \/ //_/ / __ \/ __ `/
                                                                    / /___/ / / / / ,< / / / / / /_/ / 
                                                                    /_____/_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                              /____/   

    Linking: ibex_id_stage --> controller_i
    Linking: ibex_id_stage --> registers_i
    Linking: ibex_id_stage --> decoder_i
                                                                ______   ____              __   _            
                                                                / ____/  / __ \____ _____  / /__(_)___  ____ _
                                                              / /      / /_/ / __ `/ __ \/ //_/ / __ \/ __ `/
                                                              / /___   / _, _/ /_/ / / / / ,< / / / / / /_/ / 
                                                              \____/  /_/ |_|\__,_/_/ /_/_/|_/_/_/ /_/\__, /  
                                                                                                    /____/   

    Statically analyzing target variable: csr_access_o
    Statically analyzing target variable: debug_csr_save_o
    Statically analyzing target variable: data_wdata_ex_o
    Statically analyzing target variable: data_type_ex_o
    Statically analyzing target variable: multdiv_signed_mode_ex_o
    Statically analyzing target variable: pc_mux_o
    Statically analyzing target variable: alu_operator_ex_o
    Statically analyzing target variable: id_in_ready_o
    Statically analyzing target variable: instr_valid_clear_o
    Statically analyzing target variable: csr_save_id_o
    Statically analyzing target variable: csr_restore_mret_id_o
    Statically analyzing target variable: rfvi_reg_raddr_ra_o
    Statically analyzing target variable: rfvi_reg_wdata_rd_o
    Statically analyzing target variable: rfvi_reg_rdata_ra_o
    Statically analyzing target variable: mult_en_ex_o
    Statically analyzing target variable: data_we_ex_o
    Statically analyzing target variable: csr_save_cause_o
    Statically analyzing target variable: exc_pc_mux_o
    Statically analyzing target variable: rfvi_reg_rdata_rb_o
    Statically analyzing target variable: csr_mtval_o
    Statically analyzing target variable: multdiv_operator_ex_o
    Statically analyzing target variable: debug_cause_o
    Statically analyzing target variable: exc_cause_o
    Statically analyzing target variable: data_sign_ext_ex_o
    Statically analyzing target variable: perf_tbranch_o
    Statically analyzing target variable: multdiv_operand_b_ex_o
    Statically analyzing target variable: instr_req_o
    Statically analyzing target variable: csr_save_if_o
    Statically analyzing target variable: multdiv_operand_a_ex_o
    Statically analyzing target variable: illegal_insn_o
    Statically analyzing target variable: rfvi_reg_raddr_rb_o
    Statically analyzing target variable: div_en_ex_o
    Statically analyzing target variable: debug_mode_o
    Statically analyzing target variable: pc_set_o
    Statically analyzing target variable: alu_operand_a_ex_o
    Statically analyzing target variable: alu_operand_b_ex_o
    Statically analyzing target variable: data_req_ex_o
    Statically analyzing target variable: perf_jump_o
    Statically analyzing target variable: instr_ret_o
    Statically analyzing target variable: perf_branch_o
    Statically analyzing target variable: csr_restore_dret_id_o
    Statically analyzing target variable: rfvi_reg_we_o
    Statically analyzing target variable: csr_op_o
    Statically analyzing target variable: instr_ret_compressed_o
    Statically analyzing target variable: rfvi_reg_waddr_rd_o
    Statically analyzing target variable: ctrl_busy_o


    [INFO]--> Total time for parsing, ranking: 0:00:08.516329 
    [INFO]--> Peak Memory Usage for parsing, ranking: 538.6 MB 
                                                                        _____             _            
                                                                      / ___/____ __   __(_)___  ____ _
                                                                      \__ \/ __ `/ | / / / __ \/ __ `/
                                                                      ___/ / /_/ /| |/ / / / / / /_/ / 
                                                                    /____/\__,_/ |___/_/_/ /_/\__, /  
                                                                                              /____/   

    Saving static analysis info for: ibex_id_stage
    Saving static analysis info for: ibex_controller
    Saving static analysis info for: ibex_decoder
    Saving static analysis info for: ibex_register_file


    Saving static analysis info for complete: ibex_id_stage
    [WARN]--> Cannot remove a non-existent directory: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_id_stage/static/cone 
    Saving cone for the target variable: csr_access_o
    Saving cone for the target variable: multdiv_signed_mode_ex_o
    Saving cone for the target variable: data_wdata_ex_o
    Saving cone for the target variable: data_type_ex_o
    Saving cone for the target variable: debug_csr_save_o
    Saving cone for the target variable: pc_mux_o
    Saving cone for the target variable: alu_operator_ex_o
    Saving cone for the target variable: id_in_ready_o
    Saving cone for the target variable: instr_valid_clear_o
    Saving cone for the target variable: csr_save_id_o
    Saving cone for the target variable: csr_restore_mret_id_o
    Saving cone for the target variable: rfvi_reg_raddr_ra_o
    Saving cone for the target variable: rfvi_reg_wdata_rd_o
    Saving cone for the target variable: rfvi_reg_rdata_ra_o
    Saving cone for the target variable: mult_en_ex_o
    Saving cone for the target variable: data_we_ex_o
    Saving cone for the target variable: csr_save_cause_o
    Saving cone for the target variable: exc_pc_mux_o
    Saving cone for the target variable: rfvi_reg_rdata_rb_o
    Saving cone for the target variable: csr_mtval_o
    Saving cone for the target variable: multdiv_operator_ex_o
    Saving cone for the target variable: debug_cause_o
    Saving cone for the target variable: exc_cause_o
    Saving cone for the target variable: data_sign_ext_ex_o
    Saving cone for the target variable: perf_tbranch_o
    Saving cone for the target variable: multdiv_operand_b_ex_o
    Saving cone for the target variable: instr_req_o
    Saving cone for the target variable: csr_save_if_o
    Saving cone for the target variable: multdiv_operand_a_ex_o
    Saving cone for the target variable: illegal_insn_o
    Saving cone for the target variable: rfvi_reg_raddr_rb_o
    Saving cone for the target variable: div_en_ex_o
    Saving cone for the target variable: debug_mode_o
    Saving cone for the target variable: pc_set_o
    Saving cone for the target variable: alu_operand_a_ex_o
    Saving cone for the target variable: alu_operand_b_ex_o
    Saving cone for the target variable: data_req_ex_o
    Saving cone for the target variable: perf_jump_o
    Saving cone for the target variable: instr_ret_o
    Saving cone for the target variable: perf_branch_o
    Saving cone for the target variable: csr_restore_dret_id_o
    Saving cone for the target variable: rfvi_reg_we_o
    Saving cone for the target variable: csr_op_o
    Saving cone for the target variable: instr_ret_compressed_o
    Saving cone for the target variable: rfvi_reg_waddr_rd_o
    Saving cone for the target variable: ctrl_busy_o
                                                                    ____        __           ______         
                                                                  / __ \____ _/ /_____ _   / ____/__  ____ 
                                                                  / / / / __ `/ __/ __ `/  / / __/ _ \/ __ \
                                                                / /_/ / /_/ / /_/ /_/ /  / /_/ /  __/ / / /
                                                                /_____/\__,_/\__/\__,_/   \____/\___/_/ /_/ 
                                                                                                            

    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_id_stage/ibex_id_stage_bench.v 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_id_stage/ibex_id_stage.vcd 
    [INFO]--> Simulating with: iverilog 


    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_id_stage/ibex_id_stage_compile.log 
    [INFO]--> Cannot find file: /home/vineesh/trunk/research/others/goldminer/RunTime/goldmine.out/ibex_id_stage/ibex_id_stage_err.log 
    [INFO]--> Compile Time: 0:00:01.006041 
    [INFO]--> Compile Memory Usage: 4.4 MB 


    [FAIL]--> Verilog file compilation failed. Cannot run simulation 

SOLUTION

    Downloaded Iverilog v 10.2 from [here](ftp://ftp.icarus.com/pub/eda/verilog/v10/) and installed it using the instructions from [here](https://iverilog.fandom.com/wiki/Installation_Guide)

